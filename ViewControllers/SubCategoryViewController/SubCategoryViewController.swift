

import UIKit

class SubCategoryViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var mCollectionView: UICollectionView!
    
    let sunbCategoryReuseIdentifier = "CategoryCell"
    
    var mSelectedCategoryInfo : Dictionary<String, String>?
    var mParentCategoryInfo : Dictionary<String, String>?
    var mSubCategoriesList : NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lCategoryName : String = Utils.getProperString(mSelectedCategoryInfo!["category_name"])
        self.navigationItem.title = lCategoryName
        
        settingSubCategoriesViews()
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: ---------------------  User Defined Functions --------------------
    
    
    func settingSubCategoriesViews() {
        
        mCollectionView.register(UINib(nibName: "CategoryCell", bundle:nil), forCellWithReuseIdentifier: "CategoryCell")
        
        mCollectionView.dataSource = self
        mCollectionView.delegate = self
        
        if let layout = mCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
                layout.minimumLineSpacing = 10
                layout.minimumInteritemSpacing = 10
                layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                let lWidth : CGFloat = (kWindowScreenWidth-40)/2
                let size = CGSize(width:lWidth, height: 150)
                layout.itemSize = size
        }
        
        refreshSubCategorViewcontroller()
    }
    
    func refreshSubCategorViewcontroller()
    {
        let lProductManager : ProductManager = ProductManager.sharedProductManager
        mSubCategoriesList = lProductManager.getSubCategoriesHavingCategory(mSelectedCategoryInfo)
        
        //print("SubCategories list = \(mSubCategoriesList)")
        
        DispatchQueue.main.async {
            self.mCollectionView.reloadData()
        }
    }
    
    func moveToProductListingScreenHavingInfo(_ pCategoryInfo : Dictionary<String,String>)
    {
        let lProductVC : ProductViewController = ProductViewController()
        lProductVC.mSelectedCategoryInfo = pCategoryInfo
        self.navigationController?.pushViewController(lProductVC, animated: true)
    }
    
    func moveToSubCategoryScreenHavingInfo(_ pCategoryInfo : Dictionary<String,String>)
    {
        let lSubCategoryVC :  SubCategoryViewController = SubCategoryViewController()
        lSubCategoryVC.mSelectedCategoryInfo = pCategoryInfo
        lSubCategoryVC.mParentCategoryInfo = self.mSelectedCategoryInfo
        self.navigationController?.pushViewController(lSubCategoryVC, animated: true)
    }
    
    // MARK: - UICollectionViewDelegate Datasouces and Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (Utils.isEmptyArray(mSubCategoriesList) == false) ? self.mSubCategoriesList!.count : 0;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: sunbCategoryReuseIdentifier, for: indexPath) as! CategoryCell
        if(Utils.isEmptyArray(mSubCategoriesList) == false)
        {
            let lDict : Dictionary<String,String>? = mSubCategoriesList?.object(at: indexPath.row) as? Dictionary
            if(Utils.isEmptyDictionary(lDict) == false)
            {
                cell.setUpCategoryCellHavingInfo(lDict!)
            }
        }
        
        return cell
    }
    
    
    //        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    //        {
    //            let lWidth : CGFloat = (kWindowScreenWidth - 60)/2
    //            return CGSize(width: lWidth, height: 150);
    //
    //        }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: (kWindowScreenWidth - 60)/2, height: 180);
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        
        let lDict : Dictionary<String,String>? = mSubCategoriesList?.object(at: indexPath.row) as? Dictionary
        let lChildCategories : String = Utils.getProperString(lDict!["child_categories"])
        if(Utils.isEmptyString(lChildCategories) == false)
        {
            moveToSubCategoryScreenHavingInfo(lDict!)
        }
        else
        {
            moveToProductListingScreenHavingInfo(lDict!)
        }
        
        
    }
    
}
