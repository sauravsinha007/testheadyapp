

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var mCategoryNamelbl: UILabel!
    @IBOutlet weak var mCellBaseView: UIView!
    @IBOutlet weak var mOverlayView: UIView!
    @IBOutlet weak var mCharacterLbl: UILabel!
    
    var mCategoryInfo : Dictionary<String, String>?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mCellBaseView.setLayout(WithBorderWidth: 1.0, borderColor: .lightGray, radius: 1.0, andBackgroundColor: .white)

    }

    func setUpCategoryCellHavingInfo(_ info:Dictionary<String, String>) {
        mCategoryInfo = info
        let lCategoryName : String = Utils.getProperString(info["category_name"])
        mCategoryNamelbl.text = lCategoryName
       
        mCharacterLbl.text = lCategoryName.first?.uppercased()
        
        let lBgColor : UIColor = Utils.pickColor(lCategoryName.first!)
        mOverlayView.setLayout(WithBorderWidth: 1.0, borderColor: lBgColor, radius: mOverlayView.frame.size.width/2, andBackgroundColor: lBgColor)
        
        
        
        
    }
    
}
