

import UIKit

enum EProductListingDisplayType {
    case `default`
    case mostViewedProducts
    case mostOrderedProducts
    case mostSharedProducts
}

class ProductManager: NSObject {

    var mRankingList : NSMutableArray?
    var mDisplayType:EProductListingDisplayType = .default

    
    static let sharedProductManager = ProductManager()
    fileprivate override init() {
       }
    
    func saveAllDataInLocalDb(_ pResponse: NSDictionary)
    {
        
        if(Utils.isEmptyDictionary(pResponse) == false)
        {
            let lCategoriesList : NSMutableArray? = pResponse.object(forKey: "categories") as? NSMutableArray
            
            if(Utils.isEmptyArray(lCategoriesList) == false)
            {
                for  lDict in lCategoriesList!
                {
                    let lCatigoryInfo : NSDictionary = lDict as! NSDictionary
                    if(Utils.isEmptyDictionary(lCatigoryInfo) == false)
                    {
                        self.saveCategoryInLocalDb(lCatigoryInfo)
                    }
                }
            }
            
            
            self.saveRankingDataInLocalDb(pResponse)
            
        }
    }
    
    func saveCategoryInLocalDb(_ pInfo : NSDictionary)
    {
        let lDatabaseManager :  DatabaseManager  = DatabaseManager.getInstance()
        lDatabaseManager.database?.open()
        
        let lCategoryId : Int = pInfo.value(forKey: "id") as! Int
        let lCategoryName : String = Utils.getProperString(pInfo.value(forKey: "name") as? String)
        let lChildCategories : NSArray? = pInfo.value(forKey: "child_categories") as? NSArray
        var lChildCategoriesInStr : String = ""
        if(Utils.isEmptyArray(lChildCategories) == false) {
            lChildCategoriesInStr = lChildCategories!.componentsJoined(by: ",")
        }
        
        let isSave = lDatabaseManager.database?.executeUpdate("INSERT INTO category (category_id,category_name,child_categories) VALUES (?,?,?)", withArgumentsIn: [lCategoryId,lCategoryName,lChildCategoriesInStr])
        
        if(isSave == false) {
            print("Error in db = \(lDatabaseManager.database?.lastError() ?? "Error in db inserting in category table" as! Error)")
        }
        lDatabaseManager.database?.close()

        
        let lProdcuctList : NSArray? = pInfo.object(forKey: "products") as? NSArray
        if(Utils.isEmptyArray(lProdcuctList) == false)
        {
            for  lDict in lProdcuctList!
            {
                let lProductInfo : NSDictionary = lDict as! NSDictionary
                if(Utils.isEmptyDictionary(lProductInfo) == false)
                {
                    self.saveProductsInLocalDb(lProductInfo, andCategoryInfo: pInfo)
                }
            }
        }
        
    }
    
    func saveProductsInLocalDb(_ pProductInfo : NSDictionary, andCategoryInfo pCategoryInfo:NSDictionary)
    {
        let lDatabaseManager :  DatabaseManager  = DatabaseManager.getInstance()
        lDatabaseManager.database?.open()
       
        
        let lCategoryId : Int = pCategoryInfo.value(forKey: "id") as! Int
        let lProductId : Int = pProductInfo.value(forKey: "id") as! Int
        let lProductName : String = Utils.getProperString(pProductInfo.value(forKey: "name") as? String)
        let lVariantsList : NSArray? = pProductInfo.value(forKey: "variants") as? NSArray
        let lIsVariant : Int = Utils.isEmptyArray(lVariantsList) == true ? 0 : 1
        
        
        let lTaxInfo : NSDictionary? = pProductInfo.value(forKey: "tax") as? NSDictionary
        var lTaxName : String = ""
        var lTaxValue : String = ""
        
        if(Utils.isEmptyDictionary(lTaxInfo) == false)
        {
            lTaxName  = Utils.getProperString(lTaxInfo!.value(forKey: "name") as? String)
            lTaxValue = String(format:"%.2f", lTaxInfo!.value(forKey: "value") as! Double)

        }
        
    
        let isSave = lDatabaseManager.database?.executeUpdate("INSERT INTO product (product_id,product_name,is_variants,tax_name,tax_value,category_id,view_count,order_count,shares) VALUES (?,?,?,?,?,?,?,?,?)", withArgumentsIn: [lProductId,lProductName,lIsVariant,lTaxName,lTaxValue,lCategoryId,"0","0","0"])
        
        if(isSave == false) {
            print("Error in db = \(lDatabaseManager.database?.lastError() ?? "Error in db inserting in product  table" as! Error)")
        }
        lDatabaseManager.database?.close()

        if(Utils.isEmptyArray(lVariantsList) == false)
        {
            for  lDict in lVariantsList!
            {
                let lVariantInfo : NSDictionary = lDict as! NSDictionary
                if(Utils.isEmptyDictionary(lVariantInfo) == false)
                {
                    self.saveVariantInLocalDb(lVariantInfo, andProductInfo: pProductInfo)
                }
            }
        }
    }
    
    func saveVariantInLocalDb(_ pVariantInfo : NSDictionary, andProductInfo pProductInfo:NSDictionary)
    {
        let lDatabaseManager :  DatabaseManager  = DatabaseManager.getInstance()
        lDatabaseManager.database?.open()
       
        
        let lProductId : Int = pProductInfo.value(forKey: "id") as! Int
        let lVariantId : Int = pVariantInfo.value(forKey: "id") as! Int
        let lVariantName : String = Utils.getProperString(pVariantInfo.value(forKey: "name") as? String)
        let lColor : String = Utils.getProperString(pVariantInfo.value(forKey: "color") as? String)
        let lPrice : String = String(pVariantInfo.value(forKey: "price") as! Int)
        let lSize : String = String(pVariantInfo.value(forKey: "size") as? Int ?? 0)
        
    
        let isSave = lDatabaseManager.database?.executeUpdate("INSERT INTO variant (variant_id,variant_name,color,size,price,product_id) VALUES (?,?,?,?,?,?)", withArgumentsIn: [lVariantId,lVariantName,lColor,lSize,lPrice,lProductId])
        
        if(isSave == false) {
            print("Error in db = \(lDatabaseManager.database?.lastError() ?? "Error in db inserting in variant  table" as! Error)")
        }
        lDatabaseManager.database?.close()

    }
    
    func deleteAllDataFromLocalDB()
    {
        let lDatabaseManager :  DatabaseManager  = DatabaseManager.getInstance()
        lDatabaseManager.database?.open()
        
        let lIsDeleteDataCategoryTable = lDatabaseManager.database?.executeUpdate("DELETE FROM category", withArgumentsIn: [])
        
        if(lIsDeleteDataCategoryTable == false) {
            print("Error in db = \(lDatabaseManager.database?.lastError() ?? "Error in deleteing data in category table" as! Error)")
        }
        
        let lIsDeleteDataProductTable = lDatabaseManager.database?.executeUpdate("DELETE FROM product", withArgumentsIn: [])
        
        if(lIsDeleteDataProductTable == false) {
            print("Error in db = \(lDatabaseManager.database?.lastError() ?? "Error in deleteing data in product table" as! Error)")
        }
        
        let lIsDeleteDataVariantTable = lDatabaseManager.database?.executeUpdate("DELETE FROM variant", withArgumentsIn: [])
        
        if(lIsDeleteDataVariantTable == false) {
            print("Error in db = \(lDatabaseManager.database?.lastError() ?? "Error in deleteing data in variant table" as! Error)")
        }
        
        
        lDatabaseManager.database?.close()
        
    }
    
    func getAllCategories() -> NSMutableArray? {
        
        let lCategoryList : NSMutableArray = NSMutableArray()
        
        let lDatabaseManager :  DatabaseManager  = DatabaseManager.getInstance()
        lDatabaseManager.database?.open()
        do {
            
            let rs = try lDatabaseManager.database?.executeQuery("select * from category", values: nil)
            while rs!.next() {
                
                var lDict:Dictionary<String, String> = Dictionary()
                
                let lCategoryId : String = String(rs?.int(forColumn: "category_id") ?? 0)
                let lCategoryName : String = Utils.getProperString((rs?.string(forColumn: "category_name")))
                let lChildCategories : String = Utils.getProperString((rs?.string(forColumn: "child_categories")))
                
                lDict["category_id"] = lCategoryId
                lDict["category_name"] = lCategoryName
                lDict["child_categories"] = lChildCategories
                lDict["category_type"] = "default"
                
                lCategoryList.add(lDict)
                
            }
            
        } catch {
            
        }
        
        lDatabaseManager.database?.close()
                
        return lCategoryList
    }
    
    func getSubCategoriesHavingCategory(_ pCategoryInfo : Dictionary<String,String>?) -> NSMutableArray? {
        
        let lSubCategories : NSMutableArray = NSMutableArray()
        
        let lDatabaseManager :  DatabaseManager  = DatabaseManager.getInstance()
        lDatabaseManager.database?.open()
        do {
            let lGivenChildCategories : String =  pCategoryInfo!["child_categories"]!
            let lQuery : String = "select * from category where category_id in (" + lGivenChildCategories + ")"
            let rs = try lDatabaseManager.database?.executeQuery(lQuery, values: [])
            while rs!.next() {
                
                var lDict:Dictionary<String, String> = Dictionary()
                
                let lCategoryId : String = String(rs?.int(forColumn: "category_id") ?? 0)
                let lCategoryName : String = Utils.getProperString((rs?.string(forColumn: "category_name")))
                let lChildCategories : String = Utils.getProperString((rs?.string(forColumn: "child_categories")))
                
                lDict["category_id"] = lCategoryId
                lDict["category_name"] = lCategoryName
                lDict["child_categories"] = lChildCategories
                lDict["category_type"] = "default"
                
                lSubCategories.add(lDict)
                
            }
            
        } catch {
            
        }
        
        lDatabaseManager.database?.close()
                
        return lSubCategories
    }
    
    
    func getProductsHavingCategory(_ pCategoryInfo : Dictionary<String,String>?) -> NSMutableArray? {
        
        let lProductList : NSMutableArray = NSMutableArray()
        
        let lDatabaseManager :  DatabaseManager  = DatabaseManager.getInstance()
        lDatabaseManager.database?.open()
        do {
            let lCatId : String =  pCategoryInfo!["category_id"]!
            
            let lCategoryType : String = Utils.getProperString(pCategoryInfo!["category_type"])
            
            var lQuery : String = ""
            var lOrderBy : String = ""
            if(lCategoryType == "ranking")
            {
                let lProdutIds = Utils.getProperString(pCategoryInfo!["product_ids"])
                if(self.mDisplayType == .mostViewedProducts) {
                    lOrderBy = " Order By view_count DESC"
                } else if(self.mDisplayType == .mostOrderedProducts) {
                    lOrderBy = " Order By order_count DESC"
                } else if(self.mDisplayType == .mostSharedProducts) {
                    lOrderBy = " Order By shares DESC"
                }
                
                lQuery  = "select * from product where product_id in (" + lProdutIds + ")" + lOrderBy
            }
            else
            {
                lQuery  = "select * from product where category_id = " + lCatId
            }
            
            let rs = try lDatabaseManager.database?.executeQuery(lQuery, values: [])
            
            //let rs = try lDatabaseManager.database?.executeQuery("select * from product where category_id = ?", values: [lCatId])
            while rs!.next() {
                
                var lDict:Dictionary<String, String> = Dictionary()
                
                let lProductId : String = String(rs?.int(forColumn: "product_id") ?? 0)
                let lCategoryId : String = String(rs?.int(forColumn: "category_id") ?? 0)
                let lProductName : String = Utils.getProperString((rs?.string(forColumn: "product_name")))
                let lIsVariants : String = String(rs?.int(forColumn: "is_variants") ?? 0)
                let lTaxName : String = Utils.getProperString((rs?.string(forColumn: "tax_name")))
                let lTaxValue : String = Utils.getProperString((rs?.string(forColumn: "tax_value")))
                let lViewCount : String = String(rs?.int(forColumn: "view_count") ?? 0)
                let lOrderCount : String = String(rs?.int(forColumn: "order_count") ?? 0)
                let lShareCount : String = String(rs?.int(forColumn: "shares") ?? 0)
                
                
                lDict["product_id"] = lProductId
                lDict["product_name"] = lProductName
                lDict["is_variants"] = lIsVariants
                lDict["tax_name"] = lTaxName
                lDict["tax_value"] = lTaxValue
                lDict["category_id"] = lCategoryId
                lDict["view_count"] = lViewCount
                lDict["order_count"] = lOrderCount
                lDict["shares"] = lShareCount
                
                lProductList.add(lDict)
                
            }
            
        } catch {
            
        }
        
        lDatabaseManager.database?.close()
                
        return lProductList
    }
    
    
    
    func getVariantsHavingProductInfo(_ pProductInfo : Dictionary<String,String>?) -> NSMutableArray? {
        
        let lVariantist : NSMutableArray = NSMutableArray()
        
        let lDatabaseManager :  DatabaseManager  = DatabaseManager.getInstance()
        lDatabaseManager.database?.open()
        do {
            let lGivenProductId : String =  pProductInfo!["product_id"]!
            
            let rs = try lDatabaseManager.database?.executeQuery("select * from variant where product_id = ?", values: [lGivenProductId])
            while rs!.next() {
                
                var lDict:Dictionary<String, String> = Dictionary()
                
                let lProductId : String = String(rs?.int(forColumn: "product_id") ?? 0)
                let lVariantId : String = String(rs?.int(forColumn: "variant_id") ?? 0)
                let lColor : String = Utils.getProperString((rs?.string(forColumn: "color")))
                let lPrice : String = Utils.getProperString((rs?.string(forColumn: "price")))
                let lSize : String = Utils.getProperString((rs?.string(forColumn: "size")))
                
                
                lDict["product_id"] = lProductId
                lDict["variant_id"] = lVariantId
                lDict["color"] = lColor
                lDict["price"] = lPrice
                lDict["size"] = lSize
                
                lVariantist.add(lDict)
                
            }
            
        } catch {
            
        }
        
        lDatabaseManager.database?.close()
                
        return lVariantist
    }
    
    func saveRankingDataInLocalDb(_ pResponse:NSDictionary?)
    {
        if(Utils.isEmptyDictionary(pResponse) == false)
        {
            let lRankingList : NSMutableArray? = pResponse!.object(forKey: "rankings") as? NSMutableArray
            if(Utils.isEmptyArray(lRankingList) == false)
            {
                mRankingList = NSMutableArray(array: lRankingList!)
                
                for  lDict in lRankingList!
                {
                    let lRankingInfo : NSDictionary = lDict as! NSDictionary
                    if(Utils.isEmptyDictionary(lRankingInfo) == false)
                    {
                        let lRankingName : String = Utils.getProperString(lRankingInfo.value(forKey: "ranking") as? String)
                        let lProductList : NSMutableArray? = lRankingInfo.value(forKey: "products") as? NSMutableArray
                        
                        for lTempDict in lProductList!
                        {
                            let lProductInfo : NSDictionary = lTempDict as! NSDictionary
                            let lProductId : Int = lProductInfo.value(forKey: "id") as! Int
                            var lType : String = ""
                            var lCount : Int = 0
                            
                            if(lRankingName.lowercased() == "most viewed products") {
                                lCount = lProductInfo.value(forKey: "view_count") as! Int
                                lType = "view_count"
                                
                            } else if(lRankingName.lowercased() ==  "most ordered products") {
                                lCount = lProductInfo.value(forKey: "order_count") as! Int
                                lType = "order_count"
                                
                            } else if(lRankingName.lowercased() == "most shared products") {
                                lCount = lProductInfo.value(forKey: "shares") as! Int
                                lType = "shares"

                            }
                            
                            self.updateRankingInfoInProductTable(lProductId, count: lCount, andRankingType: lType)
                        }
                                                
                    }
                }
            }
        }
        
    }
    
    func updateRankingInfoInProductTable(_ pProductId: Int , count pCount:Int, andRankingType pType:String)
    {
        let lDatabaseManager :  DatabaseManager  = DatabaseManager.getInstance()
        lDatabaseManager.database?.open()
            
        let lQuery : String = "update product set " + pType + " = " + String(pCount) + " where product_id  = " + String(pProductId)
        
        let isUpdate = lDatabaseManager.database?.executeUpdate(lQuery, withArgumentsIn: [])
        
        //let isUpdate = lDatabaseManager.database?.executeUpdate("UPDATE product set ? = ? where product_id = ?", withArgumentsIn: [pType,pCount,pProductId])
        
        if(isUpdate == false) {
            print("Error in db = \(lDatabaseManager.database?.lastError() ?? "Error in db udating table in product  table" as! Error)")
        }
        lDatabaseManager.database?.close()
    }
    
    func getDataForRankingCategories() -> NSMutableArray?
    {
        let lCatRankingList : NSMutableArray = NSMutableArray()
        if(Utils.isEmptyArray(mRankingList) == false)
        {
            for  lDict in mRankingList!
            {
                let lRankingInfo : NSDictionary = lDict as! NSDictionary
                if(Utils.isEmptyDictionary(lRankingInfo) == false)
                {
                    var lDict:Dictionary<String, String> = Dictionary()
                    var lCategoryId : String = ""
                    let lProductIdArray : NSMutableArray = NSMutableArray()
                    
                    let lRankingName : String = Utils.getProperString(lRankingInfo.value(forKey: "ranking") as? String)
                    let lProductList : NSMutableArray? = lRankingInfo.value(forKey: "products") as? NSMutableArray
                    
                    if(lRankingName.lowercased() == "most viewed products") {
                        lCategoryId = "-1"
                        
                    } else if(lRankingName.lowercased() ==  "most ordered products") {
                        lCategoryId = "-2"
                        
                    } else if(lRankingName.lowercased() == "most shared products") {
                        lCategoryId = "-3"

                    }
                    
                    for lTempDict in lProductList!
                    {
                        let lProductInfo : NSDictionary = lTempDict as! NSDictionary
                        let lProductId : Int = lProductInfo.value(forKey: "id") as! Int
                        
                        lProductIdArray.add(lProductId)
                        
                    }
                                
                    
                    
                    lDict["category_id"] = lCategoryId
                    lDict["category_name"] = lRankingName
                    lDict["child_categories"] = ""
                    lDict["category_type"] = "ranking"
                    lDict["product_ids"] = lProductIdArray.componentsJoined(by: ",")
                    
                    lCatRankingList.add(lDict)
                }
            }
        }
        
        return lCatRankingList
    }
}
