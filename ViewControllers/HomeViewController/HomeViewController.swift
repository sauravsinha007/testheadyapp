//
//  HomeViewController.swift
//
//
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var mIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mCollectionView: UICollectionView!
    
    var mCategoryList : NSMutableArray?
    let categoryReuseIdentifier = "CategoryCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Categories"
//        navigationController?.navigationBar.barTintColor = kAppBasecolor
//        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        settingHomeViews()
        //self.perform(#selector(self.settingHomeViews), with: nil, afterDelay: 3.0)

        getAllDataFromServer()
        
    }

    
    // MARK: -------------------- IBActions ---------------------------
    
    @IBAction func testButtonAction(_ sender: AnyObject)
    {
    }
    
    
    // MARK: ---------------------  User Defined Functions --------------------
    
   
    @objc func settingHomeViews() {
        
        mCollectionView.register(UINib(nibName: "CategoryCell", bundle:nil), forCellWithReuseIdentifier: "CategoryCell")

        mCollectionView.dataSource = self
        mCollectionView.delegate = self
        
        if let layout = mCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
                layout.minimumLineSpacing = 10
                layout.minimumInteritemSpacing = 10
                layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                let lWidth : CGFloat = (kWindowScreenWidth-40)/2
                let size = CGSize(width:lWidth, height: 150)
                layout.itemSize = size
        }
    }
    
    func refreshCategorViewcontroller()
    {
        let lProductManager : ProductManager = ProductManager.sharedProductManager
        
        if(mCategoryList == nil) {
            mCategoryList = NSMutableArray()
        }
        
        mCategoryList?.removeAllObjects()
        
        let lRankingCategoryList : NSMutableArray? = lProductManager.getDataForRankingCategories()
        let lDefaultCategoryList : NSMutableArray? = lProductManager.getAllCategories()
        
        if(Utils.isEmptyArray(lRankingCategoryList) == false) {
            mCategoryList?.addObjects(from: lRankingCategoryList as! [Any])
        }
        
        if(Utils.isEmptyArray(lDefaultCategoryList) == false) {
            mCategoryList?.addObjects(from: lDefaultCategoryList as! [Any])
        }
                        
        //print("Category list = \(mCategoryList)")
        
        DispatchQueue.main.async {
            
            if let layout = self.mCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
                    layout.minimumLineSpacing = 10
                    layout.minimumInteritemSpacing = 10
                    layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                    let lWidth : CGFloat = (kWindowScreenWidth-40)/2
                    let size = CGSize(width:lWidth, height: 150)
                    layout.itemSize = size
            }
            
            
            self.mCollectionView.reloadData()
        }
    }
    
    func getAllDataFromServer()
    {
        let lConnection = Connection()
        mIndicator.startAnimating()
        let lUrlStr : String = "https://stark-spire-93433.herokuapp.com/json"
        lConnection.startConnectionWithLoaderHaving(lUrlStr,methodType: "GET",postData: nil,inView:self.view,onCompletion:{responseData,responseError in
            
            if(responseError == nil) {
                do {
                    
                    let lParseOject:NSDictionary = try JSONSerialization.jsonObject(with: responseData!, options:JSONSerialization.ReadingOptions.mutableContainers ) as! NSMutableDictionary
                    
                    //print("Response = \(lParseOject)")
                    self.didReceivedResponseOfApi(lParseOject)
                    DispatchQueue.main.async {
                        self.mIndicator.stopAnimating()
                    }
                } catch {
                    // report error
                }
            }
            else {
                DispatchQueue.main.async {
                   self.mIndicator.stopAnimating()
                   self.showAlertView(withMessage: responseError!.localizedDescription, andTitle: "")
                    self.refreshCategorViewcontroller()
                }
            }
        })
    }
   
    func didReceivedResponseOfApi(_ pResponse : NSDictionary)
    {
        let lProductManager : ProductManager = ProductManager.sharedProductManager
        lProductManager.deleteAllDataFromLocalDB()
        lProductManager.saveAllDataInLocalDb(pResponse)
        
        refreshCategorViewcontroller()

    }
    
    
    func moveToProductListingScreenHavingInfo(_ pCategoryInfo : Dictionary<String,String>)
    {
        let lProductVC : ProductViewController = ProductViewController()
        lProductVC.mSelectedCategoryInfo = pCategoryInfo
        self.navigationController?.pushViewController(lProductVC, animated: true)
    }
    
    func moveToSubCategoryScreenHavingInfo(_ pCategoryInfo : Dictionary<String,String>)
    {
        let lSubCategoryVC :  SubCategoryViewController = SubCategoryViewController()
        lSubCategoryVC.mSelectedCategoryInfo = pCategoryInfo
        self.navigationController?.pushViewController(lSubCategoryVC, animated: true)
    }
    
    // MARK: - UICollectionViewDelegate Datasouces and Delegates
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            return (Utils.isEmptyArray(mCategoryList) == false) ? self.mCategoryList!.count : 0;
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            // get a reference to our storyboard cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoryReuseIdentifier, for: indexPath) as! CategoryCell
            if(Utils.isEmptyArray(mCategoryList) == false)
            {
                let lDict : Dictionary<String,String>? = mCategoryList?.object(at: indexPath.row) as? Dictionary
                if(Utils.isEmptyDictionary(lDict) == false)
                {
                    cell.setUpCategoryCellHavingInfo(lDict!)
                }
            }
            
            return cell
        }
        
        
        func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            
            
            
        }
    

        func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
         {
        
        }
 
    
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            // handle tap events
            
            let lDict : Dictionary<String,String>? = mCategoryList?.object(at: indexPath.row) as? Dictionary
            let lChildCategories : String = Utils.getProperString(lDict!["child_categories"])
            let lCategoryType : String = Utils.getProperString(lDict!["category_type"])
            
            if(lCategoryType == "ranking"
                || Utils.isEmptyString(lChildCategories) == true) {
                moveToProductListingScreenHavingInfo(lDict!)
            }
            else if(Utils.isEmptyString(lChildCategories) == false)
            {
                moveToSubCategoryScreenHavingInfo(lDict!)
            }
           
            
        }
}


extension HomeViewController: UICollectionViewDelegateFlowLayout {

  

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//          // return "String".size(withAttributes: nil)
//               let lWidth : CGFloat = (kWindowScreenWidth-40)/2
//                return CGSize(width: lWidth, height: 150)
//       }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//          let lWidth : CGFloat = (kWindowScreenWidth-40)/2
//          return CGSize(width: lWidth, height: 150)
//
//         // return CGSize(width: (kWindowScreenWidth - 60)/2, height: 180);
//
//      }
}
