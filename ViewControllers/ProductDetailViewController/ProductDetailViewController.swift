
import UIKit

class ProductDetailViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var mTaxInfoLbl: UILabel!
    @IBOutlet weak var mProductNameLbl: UILabel!
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var mVariantView: UIView!
    @IBOutlet weak var mViewCountView: UIView!
    @IBOutlet weak var mOrderCountView: UIView!
    @IBOutlet weak var mSharesCountView: UIView!
    
    
    var mSelectedProductInfo : Dictionary<String, String>?
    var mVariantList : NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lProductName : String = Utils.getProperString(mSelectedProductInfo!["product_name"])
        self.navigationItem.title = lProductName
        mProductNameLbl.text = lProductName;
        // Do any additional setup after loading the view.
        settingProductDetailViews()
    }

    // MARK: ---------------------  User Defined Functions --------------------
    
    
    func settingProductDetailViews() {
      
        mTableView.register(UINib(nibName: "VariantCell", bundle: nil), forCellReuseIdentifier: "VariantCell")
        mTableView.tableFooterView = UIView()
        mTableView.backgroundView = nil
        mTableView.backgroundColor = .white
        mTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        mTableView.dataSource = self
        mTableView.delegate = self
                    
        mViewCountView.setLayout(WithBorderWidth: 1.0, borderColor: .lightGray, radius: 1.0, andBackgroundColor: .white)

        mOrderCountView.setLayout(WithBorderWidth: 1.0, borderColor: .lightGray, radius: 1.0, andBackgroundColor: .white)

        mSharesCountView.setLayout(WithBorderWidth: 1.0, borderColor: .lightGray, radius: 1.0, andBackgroundColor: .white)

        
        refreshProductDetailViewcontroller()
    }
    
    func refreshProductDetailViewcontroller()
    {
        let lIsVariant : String = Utils.getProperString(self.mSelectedProductInfo!["is_variants"])
        
        DispatchQueue.main.async {
            
            if(lIsVariant == "1") {
                let lProductManager : ProductManager = ProductManager.sharedProductManager
                self.mVariantList = lProductManager.getVariantsHavingProductInfo(self.mSelectedProductInfo)
                self.mTableView.reloadData()
                self.mVariantView.isHidden =  false
            } else {
                self.mVariantView.isHidden  = true
            }
            
            self.mTaxInfoLbl.text =  Utils.getProperString(self.mSelectedProductInfo!["tax_name"]) + " : $" +  Utils.getProperString(self.mSelectedProductInfo!["tax_value"])
            
            
            let lViewCount : String = Utils.getProperString(self.mSelectedProductInfo!["view_count"])
            let lOrderCount : String = Utils.getProperString(self.mSelectedProductInfo!["order_count"])
            let lSharesCount : String = Utils.getProperString(self.mSelectedProductInfo!["shares"])
            
            if(Utils.isEmptyString(lViewCount) == true || lViewCount == "0")
            {
                self.mViewCountView.isHidden = true
            }
            else
            {
                let lViewCountLbl : UILabel = self.mViewCountView.viewWithTag(20) as! UILabel
                lViewCountLbl.text = lViewCount
            }
            
            if(Utils.isEmptyString(lOrderCount) == true || lOrderCount == "0")
            {
                self.mOrderCountView.isHidden = true
            }
            else
            {
                let lOrderCountLbl : UILabel = self.mOrderCountView.viewWithTag(20) as! UILabel
                lOrderCountLbl.text = lOrderCount
            }
            
            if(Utils.isEmptyString(lSharesCount) == true || lSharesCount == "0")
            {
                self.mSharesCountView.isHidden = true
            }
            else
            {
                let lShareCountLbl : UILabel = self.mSharesCountView.viewWithTag(20) as! UILabel
                lShareCountLbl.text = lSharesCount
            }
            

        }
    }

    //MARK: - TableView Datsources and Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
     
            return 1
    
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return nil
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Utils.isEmptyArray(mVariantList) == false) ? self.mVariantList!.count : 0
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let lCellIdentifier = "VariantCell"
        let cell: VariantCell? = tableView.dequeueReusableCell(withIdentifier: lCellIdentifier, for: indexPath) as? VariantCell
        cell?.tag = indexPath.row
        
        let lDict : Dictionary<String,String>? = mVariantList?.object(at: indexPath.row) as? Dictionary

        cell?.setUpVariantCellHavingInfo(lDict!)
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    
}
