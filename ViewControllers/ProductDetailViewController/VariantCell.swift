
import UIKit

class VariantCell: UITableViewCell {

    @IBOutlet weak var mColorLbl: UILabel!
    @IBOutlet weak var mColorImgView: UIImageView!
    @IBOutlet weak var mPriceLbl: UILabel!
    @IBOutlet weak var mSizeLbl: UILabel!
    @IBOutlet weak var mColorView: UIView!
    @IBOutlet weak var mSizeView: UIView!
    @IBOutlet weak var mCellBaseView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mColorView.setLayout(WithBorderWidth: 1.0, borderColor: .lightGray, radius: 1.0, andBackgroundColor: .white)
        
        mSizeView.setLayout(WithBorderWidth: 1.0, borderColor: .lightGray, radius: 1.0, andBackgroundColor: .white)
            
        mCellBaseView.setShadowCustomView()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
    
    func setUpVariantCellHavingInfo(_ info:Dictionary<String, String>) {
        let lColor : String = Utils.getProperString(info["color"])
        let lSize : String = Utils.getProperString(info["size"])
        let lPrice : String = "$" + Utils.getProperString(info["price"])
        
        mColorLbl.text = lColor
        mSizeLbl.text = lSize
        mPriceLbl.text = lPrice
        
        let lBgColor : UIColor = Utils.getColorFromString(lColor)
        
        if(lColor.lowercased() == "white") {
            mColorImgView.setLayout(WithBorderWidth: 1.0, borderColor: .lightGray, radius: mColorImgView.frame.size.width/2, andBackgroundColor: lBgColor)

        } else {
            mColorImgView.setLayout(WithBorderWidth: 1.0, borderColor: lBgColor, radius: mColorImgView.frame.size.width/2, andBackgroundColor: lBgColor)

        }
        if(Utils.isEmptyString(lSize) == true || lSize == "0") {
            mSizeView.isHidden = true
        }
        else {
             mSizeView.isHidden = false
        }
        
    }
    
}
