
import UIKit



class ProductViewController: UIViewController, UITableViewDelegate,UITableViewDataSource  {
    
    
    @IBOutlet weak var mTableView: UITableView!
    var mSelectedCategoryInfo : Dictionary<String, String>?
    var mProductList : NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lCategoryName : String = Utils.getProperString(mSelectedCategoryInfo!["category_name"])
        self.navigationItem.title = lCategoryName
        // Do any additional setup after loading the view.
        settingProductViews()
    }
    
    
    // MARK: ---------------------  User Defined Functions --------------------
    
    
    func settingProductViews() {
        
        mTableView.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        mTableView.tableFooterView = UIView()
        mTableView.backgroundView = nil
        mTableView.backgroundColor = UIColorFromRGB(0xf5f5f5)
        mTableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        mTableView.dataSource = self
        mTableView.delegate = self
        
        let lCategoryType : String = Utils.getProperString(mSelectedCategoryInfo!["category_type"])
        let lProductManager : ProductManager = ProductManager.sharedProductManager
        if(lCategoryType == "ranking")
        {
            let lRankingName : String = Utils.getProperString(mSelectedCategoryInfo!["category_name"])

            if(lRankingName.lowercased() == "most viewed products") {
                lProductManager.mDisplayType = .mostViewedProducts
                
            } else if(lRankingName.lowercased() ==  "most ordered products") {
                lProductManager.mDisplayType = .mostOrderedProducts
                
            } else if(lRankingName.lowercased() == "most shared products") {
               lProductManager.mDisplayType = .mostSharedProducts
            }
        }
        else
        {
            lProductManager.mDisplayType = .default
        }

        
        refreshProductViewcontroller()
    }
    
    func refreshProductViewcontroller()
    {
        let lProductManager : ProductManager = ProductManager.sharedProductManager
        mProductList = lProductManager.getProductsHavingCategory(mSelectedCategoryInfo)
        
        //print("Product list = \(mProductList)")
        
        DispatchQueue.main.async {
            self.mTableView.reloadData()
        }
    }
    
    func moveToProductDetailViewHavingInfo(_ pCategoryInfo : Dictionary<String,String>)
    {
        let lProductDetailVC : ProductDetailViewController = ProductDetailViewController()
        lProductDetailVC.mSelectedProductInfo = pCategoryInfo
        self.navigationController?.pushViewController(lProductDetailVC, animated: true)
    }
    
    //MARK: - TableView Datsources and Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
     
            return 1
    
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return nil
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Utils.isEmptyArray(mProductList) == false) ? self.mProductList!.count : 0
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let lProductManager : ProductManager = ProductManager.sharedProductManager
        if(lProductManager.mDisplayType == .default) {
            return 50.0
        } else {
            return 65
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let lCellIdentifier = "ProductCell"
        let cell: ProductCell? = tableView.dequeueReusableCell(withIdentifier: lCellIdentifier, for: indexPath) as? ProductCell
        cell?.tag = indexPath.row
        
        let lDict : Dictionary<String,String>? = mProductList?.object(at: indexPath.row) as? Dictionary

        cell?.setUpProductCellHavingInfo(lDict!)
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        let lDict : Dictionary<String,String>? = mProductList?.object(at: indexPath.row) as? Dictionary

        self.moveToProductDetailViewHavingInfo(lDict!)
    }
    
    
}
