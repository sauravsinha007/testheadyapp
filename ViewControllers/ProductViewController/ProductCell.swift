
import UIKit

class ProductCell: UITableViewCell {

    @IBOutlet weak var mProductNamelbl: UILabel!
    @IBOutlet weak var mCountLbl: UILabel!
    @IBOutlet weak var mCellBaseView: UIView!
    @IBOutlet weak var mOverlayView: UIView!
    @IBOutlet weak var mCharacterLbl: UILabel!
    @IBOutlet weak var mHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.accessoryType = .disclosureIndicator
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpProductCellHavingInfo(_ info:Dictionary<String, String>) {
        let lProductName : String = Utils.getProperString(info["product_name"])
        mProductNamelbl.text = lProductName
        let lProductManager : ProductManager = ProductManager.sharedProductManager
            
        if(lProductManager.mDisplayType == .mostViewedProducts) {
            mCountLbl.text = "View Count: " + Utils.getProperString(info["view_count"])
            mHeightConstraint.constant = 35
        } else if(lProductManager.mDisplayType == .mostOrderedProducts) {
            mCountLbl.text = "Order Count: " + Utils.getProperString(info["order_count"])
            mHeightConstraint.constant = 35
        } else if(lProductManager.mDisplayType == .mostSharedProducts) {
            mCountLbl.text = "Shares: " + Utils.getProperString(info["shares"])
            mHeightConstraint.constant = 35
        } else {
            mCountLbl.text = ""
            mHeightConstraint.constant = 50
        }
    }
    
}
