//
//  Constants.swift
//
//

import UIKit


let kDatabaseName = "heady_db.sqlite"


let kAppBasecolor : UIColor = UIColorFromRGB(0x20B2E0)

func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
    
}

typealias ServiceResponse = (Data?, NSError?) -> Void

let kWindowScreenSize: CGRect = UIScreen.main.bounds
let kWindowScreenWidth = kWindowScreenSize.width
let kWindowScreenHeight = kWindowScreenSize.height
