//
//  Utils.swift
//  TestKureApp
//
//

import UIKit

class Utils: NSObject {
    
    class func getDatabasePath(_ fileName: String) -> String{
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileUrl = documentDirectory.appendingPathComponent(fileName)
        print("Database Path :- \(fileUrl.path)")
        return fileUrl.path
    }
    
    class func copyDatabase(_ filename: String){
        let dbPath = getDatabasePath(kDatabaseName)
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: dbPath){
            let bundle = Bundle.main.resourceURL
            let file = bundle?.appendingPathComponent(filename)
            var error:NSError?
            do{
                try fileManager.copyItem(atPath: (file?.path)!, toPath: dbPath)
            }catch let error1 as NSError{
                error = error1
            }
            
            if error == nil{
                print("Yeah !! DB copy successfully")
            }else{
                print("error in db === \(error?.localizedDescription ?? "error in copying db")")
            }
    
        }
    }
    
    class func isEmptyString(_ string:String?) -> Bool
    {
        var lString : String? = string
        if(lString == nil || lString?.isEmpty == true) {
            return true
        }
        lString = lString?.removeWhiteSpaceFromString()
        if(lString?.characters.count == 0) {
            return true
        }
        
        if(lString == "<null>" || lString == "null" || lString == "(null)" || lString == "NULL") {
            return true
        }
        
        return false
    }
    
    class func isEmptyDictionary(_ pDict:Any?) -> Bool{
        if(pDict != nil)
        {
            if(((pDict as AnyObject).isKind(of: NSDictionary.classForCoder())))
            {
                if(((pDict as AnyObject).allKeys.count) > 0)
                {
                    return false
                }
            }
        }
        
        return true
    }
    
    class func isEmptyArray(_ pArray:Any?) -> Bool{
        if(pArray != nil)
        {
            if(((pArray as AnyObject).isKind(of: NSArray.classForCoder())))
            {
                if(((pArray as AnyObject).count)! > 0)
                {
                    return false
                }
            }
        }
        
        return true
    }
    
    
    class func getProperString(_ string:String?) -> String
    {
        var lString : String? = ""
        
        if(Utils.isEmptyString(string) == false) {
            lString = string
            lString = lString?.removeWhiteSpaceFromString()
        }
        
        return lString!
    }
    
    class func pickColor(_ alphabet: Character) -> UIColor {
        let alphabetColors = [0x5A8770, 0xB2B7BB, 0x6FA9AB, 0xF5AF29, 0x0088B9, 0xF18636, 0xD93A37, 0xA6B12E, 0x5C9BBC, 0xF5888D, 0x9A89B5, 0x407887, 0x9A89B5, 0x5A8770, 0xD33F33, 0xA2B01F, 0xF0B126, 0x0087BF, 0xF18636, 0x0087BF, 0xB2B7BB, 0x72ACAE, 0x9C8AB4, 0x5A8770, 0xEEB424, 0x407887]
        let str = String(alphabet).unicodeScalars
        let unicode = Int(str[str.startIndex].value)
        if 65...90 ~= unicode {
            let hex = alphabetColors[unicode - 65]
            return UIColor(red: CGFloat(Double((hex >> 16) & 0xFF)) / 255.0, green: CGFloat(Double((hex >> 8) & 0xFF)) / 255.0, blue: CGFloat(Double((hex >> 0) & 0xFF)) / 255.0, alpha: 1.0)
        }
        let red:CGFloat = CGFloat(drand48())
        let green:CGFloat = CGFloat(drand48())
        let blue:CGFloat = CGFloat(drand48())
        
        return UIColor(red:red, green: green, blue: blue, alpha: 1.0)
        
       /// return UIColor.black
    }
    
    class func getColorFromString(_ pStr: String) -> UIColor
    {
        let lColorStr = pStr.lowercased()
        var lColor : UIColor = .gray
        switch lColorStr {
        case "red":
            lColor = .red
        case "blue":
            lColor = .systemBlue
        case "black":
            lColor = .black
        case "white":
            lColor = .white
        case "silver":
            lColor = UIColorFromRGB(0xD3D3D3)
        case "pink":
            lColor = .systemPink
        case "green":
            lColor = .green
        case "yellow":
            lColor = .systemYellow
        case "gray":
            lColor = .gray
        case "golden":
            lColor = UIColorFromRGB(0xDAA520)
        case "brown":
            lColor = .brown
        default:
            lColor = .gray
        }
        
        return lColor
    }

    
}



//MARK: ------------------ Extensions --------------------------

extension String
{
    
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString)
    }
    
    
    func removeWhiteSpaceFromString() -> String
    {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
}


extension UIView {
    
    func setLayout(WithBorderWidth borderWidth:CGFloat, borderColor:UIColor?, radius aRadius:CGFloat, andBackgroundColor bgColor:UIColor?)
    {
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = aRadius
        if(borderColor != nil) {
            self.layer.borderColor = borderColor?.cgColor
        }
        
        if(bgColor != nil) {
            self.layer.backgroundColor = bgColor?.cgColor
        }
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
           let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
           let mask = CAShapeLayer()
           mask.path = path.cgPath
           layer.mask = mask
       }
    
    func setShadowCustomView() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.2)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.3
        self.layer.cornerRadius = 2.0
        self.backgroundColor = .white

    }
    
    
   
}


extension UITextField {
    
    func setLeftPaddingInText(_ leftPadding:CGFloat) {
       let lLeftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: leftPadding, height: self.frame.size.height))
       self.leftView = lLeftPaddingView
       self.leftViewMode = UITextField.ViewMode.always;
   }
    
    
}



extension UIViewController {
    
    func showAlertView(withMessage message: String?, andTitle title:String?){

         let lAlertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let lOkAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
        }
        
        lAlertController.addAction(lOkAction)
        self.present(lAlertController, animated: true, completion: nil)
        
    }
    
   
       
}

extension UITableView {

    func scrollToBottom(){

        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection:  self.numberOfSections-1) - 1,
                section: self.numberOfSections - 1)
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }

    func scrollToTop() {

        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
}
