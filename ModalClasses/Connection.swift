
import UIKit

class Connection: NSObject {
    
    var requsetTimeOut:TimeInterval = 15;
    var isLoaderShow : Bool = true
    var loadingText : String = "Loading..."
    var timeOutCounter : Int  = 0;
    var isAPICallAgain : Bool = true
    
    
    func startConnectionWithLoaderHaving(_ urlString:String,methodType:String,postData:Data?, inView:UIView?, onCompletion: @escaping ServiceResponse)
    {
        if(inView != nil && self.isLoaderShow == true) {
          //  Utils.showLoadingIndicatorWithTitle(loadingText, inView: inView!)
        }
        
        let url:URL = URL(string: urlString)!
        //let session = URLSession.shared
        
        // set up the session
        let config = URLSessionConfiguration.ephemeral //default
        let session = URLSession(configuration: config)
        
        var request = URLRequest(url: url)
        request.httpMethod = methodType
        request.timeoutInterval = requsetTimeOut;
        if (methodType == "POST")
        {
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = postData;
        }
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            data, response, error) in
            
            if(inView != nil && self.isLoaderShow == true) {
//                DispatchQueue.main.async {
//                    Utils.hideLoadingIndicatorFromView(inView!)
//                }
            }
            if(data != nil) {
                self.timeOutCounter = 0
                onCompletion(data!, error as NSError?)
            }
            else
            {
                print("Error response >>>> \(error) \n error?.localizedDescription = \(error?.localizedDescription)")
                if(self.isAPICallAgain == true && (error?.localizedDescription.lowercased().contains("the request timed out"))! && self.timeOutCounter == 0) {
                    self.timeOutCounter = self.timeOutCounter + 1
                    DispatchQueue.main.async {
                        self.startConnectionWithLoaderHaving(urlString, methodType: methodType, postData: postData, inView: inView, onCompletion: onCompletion)
                    }
                } else {
                    self.timeOutCounter = 0
                    let lData:Data = Data()
                    onCompletion(lData, error as NSError?)
                }
            }
            
        })
        
        task.resume()
    }
    
    
    
    
   
}
