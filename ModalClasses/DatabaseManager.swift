
import UIKit

var dBShareInstance = DatabaseManager()

class DatabaseManager: NSObject {

    
    var database:FMDatabase? = nil
       
    class func getInstance() -> DatabaseManager{
        if dBShareInstance.database == nil{
            dBShareInstance.database = FMDatabase(path: Utils.getDatabasePath(kDatabaseName))
        }
        return dBShareInstance
    }
    
    
}
